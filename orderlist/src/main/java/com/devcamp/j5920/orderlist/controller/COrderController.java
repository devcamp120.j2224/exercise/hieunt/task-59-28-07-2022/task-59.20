package com.devcamp.j5920.orderlist.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5920.orderlist.model.COrder;
import com.devcamp.j5920.orderlist.repository.IOrderRepository;


@RestController
@CrossOrigin
public class COrderController {
    @Autowired
    IOrderRepository orderRepository;
    @GetMapping("/orders")
    public ResponseEntity <List<COrder>> getAllOrders() {
        try {
            List<COrder> orderList = new ArrayList<COrder>();
            orderRepository.findAll().forEach(orderList::add);
            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
