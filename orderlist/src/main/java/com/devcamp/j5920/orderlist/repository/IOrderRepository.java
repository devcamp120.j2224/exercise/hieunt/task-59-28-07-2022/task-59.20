package com.devcamp.j5920.orderlist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j5920.orderlist.model.COrder;

public interface IOrderRepository extends JpaRepository <COrder, Long> {
    
}
